package com.viano.transport.fragment;

//import android.app.ListFragment;
import android.support.v4.app.ListFragment;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.viano.transport.LineDetailActivity;
import com.viano.transport.R;
import com.viano.transport.adapter.LineAdapter;
import com.viano.transport.database.DatabaseHelper;
import com.viano.transport.model.Line;

import java.util.ArrayList;

public class FirstSectionFragment extends ListFragment {
    private final String TAG = this.getClass().getSimpleName();

    public static FirstSectionFragment newInstance() {
        FirstSectionFragment fragment = new FirstSectionFragment();
        return fragment;
    }

    public FirstSectionFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadLineList();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_first_section, container, false);
    }

    private void loadLineList() {
        try{
            DatabaseHelper databaseHelper = new DatabaseHelper(this.getActivity());
            SQLiteDatabase database = databaseHelper.getReadableDatabase();

            String sql ="SELECT * FROM bus_line";
            Cursor cursor = database.rawQuery(sql, null);
            cursor.moveToFirst();
            ArrayList<Line> lineList = new ArrayList<Line>();
            for(int i = 0; i < cursor.getCount(); i++) {
                String line_no = cursor.getString(0);
                int bound_no = cursor.getInt(1);
                Boolean wheelchair = cursor.getInt(2)>0;
                Boolean night = cursor.getInt(3)>0;
                Boolean special = cursor.getInt(4)>0;
                Boolean racing = cursor.getInt(5)>0;
                Boolean airport = cursor.getInt(6)>0;

                String start_name_zh = cursor.getString(7);
                String start_name_en = cursor.getString(8);
                String start_name_ch = cursor.getString(9);

                String end_name_zh = cursor.getString(10);
                String end_name_en = cursor.getString(11);
                String end_name_ch = cursor.getString(12);

                String normal_first_trip = cursor.getString(13);
                String normal_last_trip = cursor.getString(14);
                String normal_time = cursor.getString(15);
                String normal_freq = cursor.getString(16);

                String holiday_first_trip = cursor.getString(17);
                String holiday_last_trip = cursor.getString(18);
                String holiday_time = cursor.getString(19);
                String holiday_freq = cursor.getString(20);

                String sat_first_trip = cursor.getString(21);
                String sat_last_trip = cursor.getString(22);
                String sat_time = cursor.getString(23);
                String sat_freq = cursor.getString(24);

                double price = cursor.getDouble(25);
                double distance = cursor.getDouble(26);
                int time = cursor.getInt(27);

                String remark_zh = cursor.getString(28);
                String remark_en = cursor.getString(29);
                String remark_ch = cursor.getString(30);
                int type = cursor.getInt(31);

                Line line = new Line();
                line.setLine_no(line_no);
                line.setBound_no(bound_no);

                line.setWheelchair(wheelchair);
                line.setNight(night);
                line.setSpecial(special);
                line.setRacing(racing);
                line.setAirport(airport);

                line.setStart_name_zh(start_name_zh);
                line.setStart_name_en(start_name_en);
                line.setStart_name_ch(start_name_ch);

                line.setEnd_name_zh(end_name_zh);
                line.setEnd_name_en(end_name_en);
                line.setEnd_name_ch(end_name_ch);

                line.setNormal_first_trip(normal_first_trip);
                line.setNormal_last_trip(normal_last_trip);
                line.setNormal_time(normal_time);
                line.setNormal_freq(normal_freq);

                line.setSat_first_trip(sat_first_trip);
                line.setSat_last_trip(sat_last_trip);
                line.setSat_time(sat_time);
                line.setSat_freq(sat_freq);

                line.setHoliday_first_trip(holiday_first_trip);
                line.setHoliday_last_trip(holiday_last_trip);
                line.setHoliday_time(holiday_time);
                line.setHoliday_freq(holiday_freq);

                line.setPrice(price);
                line.setDistance(distance);
                line.setTime(time);

                line.setRemark_zh(remark_zh);
                line.setRemark_en(remark_en);
                line.setRemark_ch(remark_ch);

                line.setType(type);

//                Log.d(TAG, "id = " + line.getId());
//                Log.d(TAG, "line_no = " + line.getLine_no());
//                Log.d(TAG, "bound_no = " + line.getBound_no());
//                Log.d(TAG, "wheelchair = " + line.isWheelchair());
//                Log.d(TAG, "night = " + line.isNight());
//                Log.d(TAG, "special = " + line.isSpecial());
//                Log.d(TAG, "racing = " + line.isRacing());
//                Log.d(TAG, "airport = " + line.isAirport());
//
//                Log.d(TAG, "start_name_zh = " + line.getStart_name_zh());
//                Log.d(TAG, "start_name_en = " + line.getStart_name_en());
//                Log.d(TAG, "start_name_ch = " + line.getStart_name_ch());
//
//                Log.d(TAG, "end_name_zh = " + line.getEnd_name_zh());
//                Log.d(TAG, "end_name_en = " + line.getEnd_name_en());
//                Log.d(TAG, "end_name_ch = " + line.getEnd_name_ch());
//
//                Log.d(TAG, "normal_first_trip = " + line.getNormal_first_trip());
//                Log.d(TAG, "normal_last_trip = " + line.getNormal_last_trip());
//                Log.d(TAG, "normal_time = " + line.getNormal_time());
//                Log.d(TAG, "normal_freq = " + line.getNormal_freq());
//
//                Log.d(TAG, "holiday_first_trip = " + line.getHoliday_first_trip());
//                Log.d(TAG, "holiday_last_trip = " + line.getHoliday_last_trip());
//                Log.d(TAG, "holiday_time = " + line.getHoliday_time());
//                Log.d(TAG, "holiday_freq = " + line.getHoliday_freq());
//
//                Log.d(TAG, "sat_first_trip = " + line.getSat_first_trip());
//                Log.d(TAG, "sat_last_trip = " + line.getSat_last_trip());
//                Log.d(TAG, "sat_time = " + line.getSat_time());
//                Log.d(TAG, "sat_freq = " + line.getSat_freq());
//
//                Log.d(TAG, "remark_zh = " + line.getRemark_zh());
//                Log.d(TAG, "remark_en = " + line.getRemark_en());
//                Log.d(TAG, "remark_ch = " + line.getRemark_ch());
//
//                Log.d(TAG, "type = " + line.getType());

                lineList.add(line);
                cursor.moveToNext();
            }

            Log.d(TAG, "size = " + lineList.size());
            LineAdapter adapter = new LineAdapter(getActivity(), lineList);
            setListAdapter(adapter);

            databaseHelper.close();
            cursor.close();
            database.close();

        } catch(Exception e) {
            Log.d(TAG, e.getMessage());
        }
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        Log.d(TAG, "position = " + position);
        Intent intent = new Intent(getActivity().getApplicationContext(), LineDetailActivity.class);
        //intent.putExtra("id", id);
        startActivity(intent);
    }

}
