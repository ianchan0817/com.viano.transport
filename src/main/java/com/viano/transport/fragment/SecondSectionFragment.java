package com.viano.transport.fragment;

import android.os.Bundle;
//import android.app.Fragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.viano.transport.R;

/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 *
 */
public class SecondSectionFragment extends Fragment {
    public static SecondSectionFragment newInstance() {
        SecondSectionFragment fragment = new SecondSectionFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public SecondSectionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second_section, container, false);
    }


}
