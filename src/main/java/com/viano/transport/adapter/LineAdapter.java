package com.viano.transport.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.viano.transport.R;
import com.viano.transport.model.Line;

import java.util.ArrayList;

/**
 * Created by ian.chan on 23/6/14.
 */
public class LineAdapter extends BaseAdapter {
    private final String TAG = this.getClass().getSimpleName();

    private ArrayList<Line> lineList = null;
    private Context context;

    public LineAdapter(Context context, ArrayList<Line> lineList) {
        this.lineList = lineList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return lineList.size();
    }

    @Override
    public Object getItem(int i) {
        return lineList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return lineList.get(i).getId();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            LayoutInflater mInflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = mInflater.inflate(R.layout.listview_first_section, null);
        }
        TextView line_no = (TextView) view.findViewById(R.id.first_section_name);
        TextView start = (TextView) view.findViewById(R.id.first_section_start);
        TextView end = (TextView) view.findViewById(R.id.first_section_end);

        line_no.setText(lineList.get(i).getLine_no());
        start.setText(lineList.get(i).getStart_name_zh());
        end.setText(lineList.get(i).getEnd_name_zh());

        return view;
    }
}
