package com.viano.transport.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by ian.chan on 19/6/14.
 */
public class DatabaseHelper extends SQLiteOpenHelper {
    private final String TAG = this.getClass().getSimpleName();
    private static final String DATABASE_NAME = "transport.sqlite";
    private static final int DATABASE_VERSION = 1;
    private static String DATABASE_PATH = "";

    private final Context mContext;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        mContext = context;

        if(android.os.Build.VERSION.SDK_INT >= 17){
            DATABASE_PATH = context.getApplicationInfo().dataDir + "/databases/";
        } else {
            DATABASE_PATH = "/data/data/" + context.getPackageName() + "/databases/";
        }
        Log.d(TAG, "DATABASE_PATH = " + DATABASE_PATH);

        File dbFile = new File(DATABASE_PATH);
        Log.d(TAG, "exist = " + dbFile.exists());
        if (!dbFile.exists()) {
            dbFile.mkdirs();
            copyDataBase();
        } else {
            SQLiteDatabase.openDatabase(DATABASE_PATH + DATABASE_NAME, null, SQLiteDatabase.OPEN_READWRITE);
        }

    }

    private void copyDataBase() {
        InputStream iStream = null;
        OutputStream oStream = null;
        String outFilePath = DATABASE_PATH + DATABASE_NAME;
        Log.d(TAG, "outFilePath = " + outFilePath);
        try {
            iStream = mContext.getAssets().open(DATABASE_NAME);
            oStream = new FileOutputStream(outFilePath);
            byte[] buffer = new byte[2048];
            int length;
            while ((length = iStream.read(buffer)) > 0) {
                oStream.write(buffer, 0, length);
            }
            oStream.flush();
            oStream.close();
            iStream.close();
        } catch (IOException e) {
            Log.e(TAG, e.getMessage());
        }
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        Log.d(TAG, "onCreate");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i2) {
        Log.d(TAG, "onUpgrade");
    }
}
