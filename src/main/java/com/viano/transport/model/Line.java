package com.viano.transport.model;

/**
 * Created by ian.chan on 23/6/14.
 */
public class Line {
    private int id;
    private String line_no;
    private int bound_no;
    private boolean wheelchair;
    private boolean night;
    private boolean circular;
    private boolean special;
    private boolean racing;
    private boolean airport;

    private String start_name_zh;
    private String start_name_en;
    private String start_name_ch;

    private String end_name_zh;
    private String end_name_en;
    private String end_name_ch;

    private String normal_first_trip;
    private String normal_last_trip;
    private String normal_time;
    private String normal_freq;

    private String sat_first_trip;
    private String sat_last_trip;
    private String sat_time;
    private String sat_freq;

    private String holiday_first_trip;
    private String holiday_last_trip;
    private String holiday_time;
    private String holiday_freq;

    private double price;
    private double distance;
    private int time;

    private String remark_zh;
    private String remark_en;
    private String remark_ch;

    private int type;

    public Line() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLine_no() {
        return line_no;
    }

    public void setLine_no(String line_no) {
        this.line_no = line_no;
    }

    public String getStart_name_zh() {
        return start_name_zh;
    }

    public void setStart_name_zh(String start_name_zh) {
        this.start_name_zh = start_name_zh;
    }

    public String getStart_name_en() {
        return start_name_en;
    }

    public void setStart_name_en(String start_name_en) {
        this.start_name_en = start_name_en;
    }

    public String getStart_name_ch() {
        return start_name_ch;
    }

    public void setStart_name_ch(String start_name_ch) {
        this.start_name_ch = start_name_ch;
    }

    public String getEnd_name_zh() {
        return end_name_zh;
    }

    public void setEnd_name_zh(String end_name_zh) {
        this.end_name_zh = end_name_zh;
    }

    public String getEnd_name_en() {
        return end_name_en;
    }

    public void setEnd_name_en(String end_name_en) {
        this.end_name_en = end_name_en;
    }

    public String getEnd_name_ch() {
        return end_name_ch;
    }

    public void setEnd_name_ch(String end_name_ch) {
        this.end_name_ch = end_name_ch;
    }

    public String getRemark_zh() {
        return remark_zh;
    }

    public void setRemark_zh(String remark_zh) {
        this.remark_zh = remark_zh;
    }

    public String getRemark_en() {
        return remark_en;
    }

    public void setRemark_en(String remark_en) {
        this.remark_en = remark_en;
    }

    public String getRemark_ch() {
        return remark_ch;
    }

    public void setRemark_ch(String remark_ch) {
        this.remark_ch = remark_ch;
    }

    public boolean isAirport() {
        return airport;
    }

    public void setAirport(boolean airport) {
        this.airport = airport;
    }

    public boolean isCircular() {
        return circular;
    }

    public void setCircular(boolean circular) {
        this.circular = circular;
    }

    public boolean isRacing() {
        return racing;
    }

    public void setRacing(boolean race) {
        this.racing = race;
    }

    public boolean isSpecial() {
        return special;
    }

    public void setSpecial(boolean special) {
        this.special = special;
    }

    public boolean isNight() {
        return night;
    }

    public void setNight(boolean night) {
        this.night = night;
    }
    public String getHoliday_first_trip() {
        return holiday_first_trip;
    }

    public void setHoliday_first_trip(String holiday_first_trip) {
        this.holiday_first_trip = holiday_first_trip;
    }

    public int getBound_no() {
        return bound_no;
    }

    public void setBound_no(int bound_no) {
        this.bound_no = bound_no;
    }

    public boolean isWheelchair() {
        return wheelchair;
    }

    public void setWheelchair(boolean wheelchair) {
        this.wheelchair = wheelchair;
    }

    public String getNormal_first_trip() {
        return normal_first_trip;
    }

    public void setNormal_first_trip(String normal_first_trip) {
        this.normal_first_trip = normal_first_trip;
    }

    public String getNormal_last_trip() {
        return normal_last_trip;
    }

    public void setNormal_last_trip(String normal_last_trip) {
        this.normal_last_trip = normal_last_trip;
    }

    public String getNormal_time() {
        return normal_time;
    }

    public void setNormal_time(String normal_time) {
        this.normal_time = normal_time;
    }

    public String getNormal_freq() {
        return normal_freq;
    }

    public void setNormal_freq(String normal_freq) {
        this.normal_freq = normal_freq;
    }

    public String getSat_first_trip() {
        return sat_first_trip;
    }

    public void setSat_first_trip(String sat_first_trip) {
        this.sat_first_trip = sat_first_trip;
    }

    public String getSat_last_trip() {
        return sat_last_trip;
    }

    public void setSat_last_trip(String sat_last_trip) {
        this.sat_last_trip = sat_last_trip;
    }

    public String getSat_time() {
        return sat_time;
    }

    public void setSat_time(String sat_time) {
        this.sat_time = sat_time;
    }

    public String getSat_freq() {
        return sat_freq;
    }

    public void setSat_freq(String sat_freq) {
        this.sat_freq = sat_freq;
    }

    public String getHoliday_last_trip() {
        return holiday_last_trip;
    }

    public void setHoliday_last_trip(String holiday_last_trip) {
        this.holiday_last_trip = holiday_last_trip;
    }

    public String getHoliday_time() {
        return holiday_time;
    }

    public void setHoliday_time(String holiday_time) {
        this.holiday_time = holiday_time;
    }

    public String getHoliday_freq() {
        return holiday_freq;
    }

    public void setHoliday_freq(String holiday_freq) {
        this.holiday_freq = holiday_freq;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public double getPrice() {

        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }


}
